from django.urls import path, include
from tastypie.api import Api

from django_voipstack_api.django_voipstack_api.api.resources import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.api.resources import *

v1_api = Api(api_name="v1")

# API GENERAL
v1_api.register(ApiAuthCustomerApiKeyResource())
v1_api.register(ApiAuthCustomerResource())

# API VOIP
v1_api.register(JobResource())
v1_api.register(MandatoryAttrsResource())
v1_api.register(TrunkResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
