from collections import OrderedDict

import requests
from django.db import transaction, connections
from django.forms.models import model_to_dict
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_asterisk_models.django_asterisk_models import models as asterisk
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.authentification import (
    ApiAuthCustomerAuthentication,
)
from django_voipstack_actions.django_voipstack_actions.func import add_job_option
from django_voipstack_actions.django_voipstack_actions.models import *
from django_voipstack_trunks.django_voipstack_trunks.models import *


def create_contract(customer_nr, article, count: int):
    r = requests.post(
        settings.CONTRACT_MANAGER_API_URL + "/v1/contract",
        headers={
            "Authorization": "ApiKey "
            + settings.CONTRACT_MANAGER_API_USER
            + ":"
            + settings.CONTRACT_MANAGER_API_PASSWD,
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        json={"customer": customer_nr, "article": article, "article_count": count},
    )

    return r.json()


class JobResource(Resource):
    class Meta:
        queryset = VoipJob.objects.all()
        always_return_data = True
        allowed_methods = ["get", "post"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        job_id = str(kwargs["pk"])

        if not job_id.isdigit():
            # wrong format
            return

        return VoipJob.objects.get(id=job_id)

    def obj_get_list(self, bundle, **kwargs):
        return VoipJob.objects.all()

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = OrderedDict()

            for item in bundle.obj:
                bundle.data[item.id] = model_to_dict(item)

        return bundle

    def obj_create(self, bundle, **kwargs):
        bundle.data["action_id"] = int(bundle.data["action_id"])
        loops = 1

        if bundle.data["action_id"] in (4, 6):
            loops = 1
        elif bundle.data["action_id"] == 10:
            # WBCI Export
            loops = 1
            if bundle.data["voip_quantity"] == 1:
                # Don't set voip_quantity for single numbers
                del bundle.data["voip_quantity"]
        else:
            if "voip_quantity" in bundle.data:
                loops = int(bundle.data["voip_quantity"])
                bundle.data["voip_quantity"] = 1

        # start database transaction to prevent broken loops
        with transaction.atomic():
            # loop for the count of new jobs
            for _ in range(loops):
                # check if job requires transact_referenceno_partner
                if bundle.data["action_id"] in (1, 3, 4, 5, 6):
                    # check if new transact_referenceno_partner is needed
                    if bundle.data["transact_referenceno_partner"] == "new":
                        # create new contract and get code
                        article = settings.CONTRACT_MGMT_ARTICLE_TELNUM

                        if bundle.data["action_id"] == 1:
                            article = settings.CONTRACT_MGMT_ARTICLE_TRUNK

                        contract = create_contract(
                            bundle.data["customer_nr"], article, 1
                        )
                        bundle.data["transact_referenceno_partner"] = contract[
                            "new_contracts"
                        ][0]["code"]

                # create new job
                new_job = VoipJob(
                    action_id=int(bundle.data["action_id"]),
                    date_added=datetime.now(),
                    user_id=1,
                    job_state_id=1,
                )

                new_job.save()

                for key, value in bundle.data.items():
                    if key == "action_id":
                        continue

                    add_job_option(new_job, key, value)

        return bundle


class MandatoryAttrsResource(Resource):
    class Meta:
        queryset = VoipActionsMandatoryAttrs.objects.all()
        always_return_data = True
        allowed_methods = ["get"]
        limit = 0
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get_list(self, bundle, **kwargs):
        return VoipActions.objects.all()

    def obj_get(self, bundle, **kwargs):
        action_id = str(kwargs["pk"])

        if not action_id.isdigit():
            # wrong format
            return

        return VoipActions.objects.get(pk=action_id)

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = OrderedDict()

            bundle.data[bundle.obj.pk] = list()

            action_id_tracker = dict()
            action_id_tracker["attribute"] = "action_id"
            action_id_tracker["settings"] = dict()
            action_id_tracker["settings"]["default_value"] = bundle.obj.pk
            action_id_tracker["settings"]["html_input_type"] = "hidden"
            action_id_tracker["settings"]["form_order"] = -1
            action_id_tracker["values"] = list()

            bundle.data[bundle.obj.pk].append(action_id_tracker)

            for attribute in bundle.obj.voipactionsmandatoryattrs_set.all():
                row = dict()
                row["attribute"] = attribute.voipjobattributes.name
                row["settings"] = dict()
                row["settings"]["form_order"] = attribute.form_order

                if attribute.default_value is None:
                    row["settings"][
                        "default_value"
                    ] = attribute.voipjobattributes.default_value
                else:
                    row["settings"]["default_value"] = attribute.default_value

                if attribute.html_input_type is None:
                    row["settings"][
                        "html_input_type"
                    ] = attribute.voipjobattributes.html_input_type
                else:
                    row["settings"]["html_input_type"] = attribute.html_input_type

                row["values"] = list()

                for (
                    attribute_value
                ) in attribute.voipjobattributes.voipjobattributevalues_set.all():
                    value = dict()
                    value["name"] = attribute_value.name
                    value["value"] = attribute_value.value

                    row["values"].append(value)

                bundle.data[bundle.obj.pk].append(row)

        return bundle


class TrunkResource(Resource):
    class Meta:
        queryset = VoipTrunks.objects.all()
        always_return_data = True
        allowed_methods = ["get"]
        limit = 0
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        trunk_id = str(kwargs["pk"])

        if not trunk_id.isdigit():
            # wrong format
            return

        return VoipTrunks.objects.get(id=trunk_id)

    def obj_get_list(self, bundle, **kwargs):
        return VoipTrunks.objects.all()

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = OrderedDict(model_to_dict(bundle.obj))

            bundle.data["customer"] = model_to_dict(bundle.obj.customer)

            if bundle.obj.tech.prefix == "sip":
                # Connect to DBs
                ast_trunk_db = VoipTrunkAsteriskDb.objects.all().order_by("id").first()

                database = ast_trunk_db.name

                new_database = dict()
                new_database["id"] = database
                new_database["NAME"] = ast_trunk_db.db_dbase
                new_database["ENGINE"] = "django.db.backends.mysql"
                new_database["USER"] = ast_trunk_db.db_user
                new_database["PASSWORD"] = ast_trunk_db.db_passwd
                new_database["HOST"] = ast_trunk_db.db_ip
                new_database["PORT"] = "3306"
                new_database["OPTIONS"] = {
                    "init_command": "SET sql_mode='STRICT_TRANS_TABLES'"
                }
                connections.databases[database] = new_database

                try:
                    trunk_settings = asterisk.PsEndpoints.objects.using(database).get(
                        id=bundle.obj.trunk_name
                    )
                    bundle.data["settings"] = model_to_dict(trunk_settings)
                except asterisk.PsEndpoints.DoesNotExist:
                    pass

        return bundle
